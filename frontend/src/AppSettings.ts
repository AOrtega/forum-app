export const server = 'https://localhost:44372';

export const webAPIUrl = `${server}/api`;

export const authSettings = {
  domain: 'dev-s7a-2x5i.us.auth0.com',
  client_id: 'nMXP1i6dL0JewxSK3L2lc8VCd2f01hfN',
  redirect_uri: window.location.origin + '/signin-callback',
  scope: 'openid profile QandAAPI email',
  audience: 'https://qanda',
};
